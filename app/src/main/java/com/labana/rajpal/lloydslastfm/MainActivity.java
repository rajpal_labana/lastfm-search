package com.labana.rajpal.lloydslastfm;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
{
    private ImageButton searchBtn;
    private ProgressBar pb;

    private JSONArray albums;
    private List<Album> albumsList;
    private int maxPages;
    private int pageLimit = 50;
    private int pageNo = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BindSearchFunctionality();
    }

    private void BindSearchFunctionality()
    {
        pb = findViewById(R.id.progressBar);
        searchBtn = findViewById(R.id.btnSearch);
        final EditText edit_txt = findViewById(R.id.txtSearch);

        //Auto trigger search submit if user selectes dont from the keyboard
        edit_txt.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                if ((actionId == EditorInfo.IME_ACTION_DONE))
                {
                    searchBtn.performClick();
                    return true;
                }
                return false;
            }
        });

        //Trigger search and gather data frm api call to last.fm
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Utilities.hideKeyboard(MainActivity.this);
                String apiKey = getResources().getString(R.string.lastfm_apikey);
                String searchText = edit_txt.getText().toString();

                if(searchText.length() > 0) {
                    new ParseData().execute("http://ws.audioscrobbler.com/2.0/?method=album.search&album="
                            + searchText + "&api_key=" + apiKey + "&format=json&limit=" + pageLimit + "&page=" + pageNo);
                }
            }
        });

        //Bind both next adn previous button for simple pagination
        ImageButton btnNext = findViewById(R.id.btnNext);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClickNext();
            }
        });

        ImageButton btnPrevious = findViewById(R.id.btnPrevious);
        btnPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClickPrevious();
            }
        });
    }

    private void ClickNext()
    {
        if(pageNo < maxPages)
        {
            pageNo++;
            searchBtn.performClick();
        }
    }

    private void ClickPrevious()
    {
        if(pageNo > 1)
        {
            pageNo--;
            searchBtn.performClick();
        }
    }

    private class ParseData extends AsyncTask<String, Void, Void>
    {
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();

            //Set progress to indicate data is loading
            pb.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(String... params) {
            HttpURLConnection conn = null;
            BufferedReader reader = null;

            try
            {
                //Connect to the external url and read and store all returned json data to a string
                URL url = new URL(params[0]);
                conn = (HttpURLConnection) url.openConnection();
                conn.connect();

                InputStream stream = conn.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }

                String jsonString = buffer.toString();

                //Parse all the returned json data
                if (jsonString != null)
                {
                    try
                    {
                        JSONObject jsonResults = new JSONObject(jsonString).getJSONObject("results");
                        int totalResults =  Integer.parseInt(jsonResults.getString("opensearch:totalResults"));
                        maxPages = totalResults / pageLimit; //A simple check to figure out the total possible pages for pagination

                        JSONObject jsonAlbumMatches = jsonResults.getJSONObject("albummatches");
                        albums = jsonAlbumMatches.getJSONArray("album");

                        //Loop through and create album array for all valid results
                        albumsList = new ArrayList<>();
                        for(int i = 0; i < albums.length(); i++)
                        {
                            JSONObject album = albums.getJSONObject(i);
                            JSONArray image = album.getJSONArray("image");
                            JSONObject imgSmall = image.getJSONObject(0);
                            JSONObject imgLarge = image.getJSONObject(2);
                            Album a = new Album();

                            a.setName(album.getString("name"));
                            a.setArtist(album.getString("artist"));
                            a.setUrl(album.getString("url"));
                            a.setSmallImgUrl(imgSmall.getString("#text"));
                            a.setLargeImgUrl(imgLarge.getString("#text"));

                            albumsList.add(a);
                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                return null;

            }
            catch (MalformedURLException e) {
                e.printStackTrace();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            //Make sure all connection and readers have been safely disconnect/closed
            finally
            {
                if (conn != null) {
                    conn.disconnect();
                }
                try
                {
                    if (reader != null)  {
                        reader.close();
                    }
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            //Hide progress now that the core loading is done and populate the RecyclerView with the parsed data
            pb.setVisibility(View.INVISIBLE);
            DisplayResults();
        }
    }

    private void DisplayResults()
    {
        RecyclerView recyclerView = findViewById(R.id.albumRecyclerView);
        recyclerView.setHasFixedSize(true);

        //Create an adapter and pass through the list of Albums
        AlbumAdapter mAdapter = new AlbumAdapter(albumsList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        TextView pageCount = findViewById(R.id.txtPages);
        pageCount.setText("Page " + pageNo + " of " + maxPages);
    }
}
