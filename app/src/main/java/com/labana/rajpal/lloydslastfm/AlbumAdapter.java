package com.labana.rajpal.lloydslastfm;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.MyViewHolder>
{
    private List<Album> albumsList;

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public ImageView albumImg;
        public TextView name;

        public MyViewHolder(View view)
        {
            super(view);

            albumImg = view.findViewById(R.id.imgAlbum);
            name = view.findViewById(R.id.txtName);
        }
    }


    public AlbumAdapter(List<Album> albumsList) {
        this.albumsList = albumsList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(final ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.album_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position)
    {
        final Context mContext = holder.itemView.getContext();
        final Album album = albumsList.get(position);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Inflate a custom layout to be used as a popup
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                View view = LayoutInflater.from(mContext).inflate(R.layout.album_details_popup, null);
                builder.setView(view);

                //Bind all relevant views
                ImageView albumImg = view.findViewById(R.id.imgLargeAlbum);
                TextView name = view.findViewById(R.id.txtName);
                TextView artist = view.findViewById(R.id.txtArtist);
                TextView url = view.findViewById(R.id.txtUrl);

                //Download image and set text
                new DownloadImageTask(albumImg).execute(album.getLargeImgUrl());
                Resources res = mContext.getResources();
                name.setText(res.getString(R.string.album_name) + " " + album.getName());
                artist.setText(res.getString(R.string.artist_name) + " " + album.getArtist());
                url.setText(res.getString(R.string.more_info) + " " + album.getUrl());

                builder.create();
                builder.show();
            }
        });


        if(album.getSmallImgUrl().isEmpty())
        {
            holder.albumImg.setImageResource(android.R.drawable.alert_dark_frame);
        }
        else
        {
            //Use Picasso to seamlessly load in and cache all the loaded images
            Picasso.with(holder.itemView.getContext())
                    .load(album.getSmallImgUrl())
                    .error(android.R.drawable.alert_dark_frame)
                    .placeholder(android.R.drawable.alert_dark_frame)
                    .fit()
                    .into(holder.albumImg);
        }

        holder.name.setText(album.getName());
    }

    @Override
    public int getItemCount() {
        return albumsList.size();
    }
}
